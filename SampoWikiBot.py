#!/usr/bin/env python3.6

#Desc: Telegram bot. Python 3.6
#Author: Dimitry Lukin, dimitry.lukin@gmail.com
#Version: 2020051500

from telegram.ext import Updater, MessageHandler, Filters
import re

import pycurl
import urllib.parse
from io import BytesIO
import pymorphy2

import logging

# telegram SampoWikiBot TOKEN
from SampoWikiBotTOKEN import TOKEN
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

SIGNIFICANTWORDS = 4
SREF='https://sampo-wiki.club/doku.php?do=search&id=start&sf=1&q='
STOPLIST = ['кто', 'что', 'день', 'добрый', 'вечер', 'округа', 'прямой', 'сампо', 'какой', 'сосед', "привет", "я", "он", "здесь", 'она', 'куда', 'туда', 'сейчас', 'оно', 'мы', 'наш', 'кто-нибудь', 'сам', 'время', 'это', 'поиск', 'ребята', 'бот', 'друг', 'куча', 'человек', 'проблема', 'смысл', 'хозяин', 'страница', 'обращение', 'квартира', 'номер', 'яндекс', 'очередь', 'пол', 'число', 'дом', 'утро']


morph = pymorphy2.MorphAnalyzer()


def xmlmethod(mtype, mstring):
    if (mtype == 'search'): 
        request='<?xml version="1.0"?><methodCall><methodName>dokuwiki.search</methodName><params><param><value><string>'+mstring+'</string></value></param></params></methodCall>'
    elif (mtype == 'pageList'):
        request='<?xml version="1.0"?><methodCall><methodName>dokuwiki.getPagelist</methodName><params><param><value><string></string></value></param></params></methodCall>'
    response = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.HTTPHEADER, ["Content-type: text/xml"])
    c.setopt(c.URL, 'http://127.0.0.1/lib/exe/xmlrpc.php')
    c.setopt(c.WRITEFUNCTION, response.write)
    c.setopt(c.POSTFIELDS, request.encode('utf-8'))
    c.perform()
    c.close()
    r = response.getvalue()
    response.close()
    return r.decode('utf-8')

def getbase(w):
    nword = w.lower()
    parsed = morph.parse(nword)[0]
#    print("TAGS > ", parsed.tag)
    if ( 'NOUN' or 'ADJF' ) in parsed.tag:
        w = parsed.normal_form
    else:
        w = 'GFk8wadf612'
#    print("Get Base > ", w)
    return w


def getwiki(search):
    s = xmlmethod('pageList', search)
    for word in s.split():
        if search == word:
            return True
#At first it looks for 'search' in page titles. If it found there, there is no need to make fulltext search.
    if (xmlmethod('pageList', search).find(search) > -1):
        return True
    if (xmlmethod('search', search).find('member') > -1):
        return True
    return False

def isStopWord(word):
    for i in range(0, len(STOPLIST)):
        if word.lower() == STOPLIST[i]:
            return True
    return False


def hMessage(update, context):
    phrase=update.message.text
    if phrase.count('?') < 1:
        return	

    print("INPUT > ",phrase)
    cPhrase=re.sub('[?.,!#$@:\n]',' ', phrase)
    words = cPhrase.split(" ")
    print("WORDS > ", words)
    counter=0
    for word in words:
        if len(word) < 4:
            continue
        nword = getbase(word)
        if isStopWord(nword):
            continue
        if counter > SIGNIFICANTWORDS:
            continue
        counter=counter+1
        if getwiki(nword) is True:
            context.bot.send_message(chat_id=update.message.chat_id, text='*'+word+'*'+'   Есть бесполезный ответ  -> '+ SREF+nword)
            print("OUTPUT > ", '*'+word+'*'+'   Есть ответ  -> '+ SREF+nword)

def main():
    updater = Updater(TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    handle_message = MessageHandler(Filters.text, hMessage)
    dispatcher.add_handler(handle_message)
    updater.start_polling()
    updater.idle()
if __name__ == '__main__':
    main()

